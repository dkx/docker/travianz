#!/usr/bin/env bash

set -e

if [[ ! -f var/installed ]]; then
	echo "Waiting to finish the installation..." >> /dev/stdout

	while [[ ! -f var/installed ]]
	do
	  sleep 2
	done
fi

if [[ -d install ]]; then
	echo "Removing installation data..." >> /dev/stdout
	rm -rf install
fi

sudo chmod -R 755 GameEngine
sudo chmod -R 777 GameEngine/Prevention
sudo chmod -R 777 GameEngine/Notes
sudo chmod -R 777 var/log
