#!/usr/bin/env bash

set -e

if [[ ! -f .__source_ready ]]; then
	echo "Copying travianz source to /var/www/html..."
	rsync -r --append-verify /var/www/travian/ /var/www/html/
	chown -R www-data:www-data /var/www/html
	chmod -R 777 install
	chmod -R 777 GameEngine
	touch .__source_ready
	echo "done"
fi

. /etc/apache2/envvars

/postinstall.sh &

apache2 -DFOREGROUND
