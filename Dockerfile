ARG arch

FROM balenalib/${arch}-ubuntu:bionic
LABEL maintainer="David Kudera <kudera.d@gmail.com>"

ARG travianzVersion

ENV DEBIAN_FRONTEND="noninteractive"

# cross-build-start

RUN apt update && \
	apt install --no-install-recommends -yq \
		curl ca-certificates rsync \
		php libapache2-mod-php php-mysql php-mbstring \
		apache2 && \
	curl -L -o travianz.tar.gz https://github.com/iopietro/Travianz/archive/${travianzVersion}.tar.gz && \
	tar -xzf travianz.tar.gz && \
	rm travianz.tar.gz && \
	mv Travianz-${travianzVersion} /var/www/travian && \
	chown -R www-data:www-data /var/www/travian && \
	chmod -R 777 /var/www/travian/install && \
	chmod -R 777 /var/www/travian/GameEngine && \
	apt-get -y autoremove && apt-get clean && apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./config/php.ini /etc/php/7.2/apache2/conf.d/10-errors.ini
COPY ./config/server-name.conf /etc/apache2/conf-enabled/server-name.conf
COPY ./config/vhost.conf /etc/apache2/sites-enabled/000-travianz.conf
COPY ./bin/start.sh /start.sh
COPY ./bin/postinstall.sh /postinstall.sh

RUN chmod +x /start.sh /postinstall.sh

WORKDIR /var/www/html

# cross-build-end

EXPOSE 80
VOLUME ["/var/www/html"]
CMD ["/start.sh"]
